#include "SubwaterMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


SubwaterMaterial::SubwaterMaterial(std::string name):
	MaterialGL(name,"SubwaterMaterial")
{

	light_position = vp->uniforms()->getGPUvec3("light_position");
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	camera_position = vp->uniforms()->getGPUvec3("camera_position");

	color = fp->uniforms()->getGPUvec4("CPU_color");
	light_coeffs = fp->uniforms()->getGPUvec3("light_coeffs");
	time = vp->uniforms()->getGPUfloat("time");
	depth = vp->uniforms()->getGPUvec3("depth");
	water_pos = vp->uniforms()->getGPUvec3("water_pos");

	light_position->Set(glm::vec3(0.,10000,0)); 
	light_coeffs->Set(glm::vec3(0.4, 0.6, 0.));
	camera_position->Set(glm::vec3(100,50,.0));
	time->Set(0.f);
	depth->Set(glm::vec3(0.,-10.,0.));
	water_pos->Set(glm::vec3(0.));
	color->Set(glm::vec4(153./255., 102./255., 0./255., 1.));

	sampler = fp->uniforms()->getGPUsampler("tex");
	//sampler->Set(GPUTexture2D(ressourceTexPath + "sand.jpg").getHandle());
}
SubwaterMaterial::~SubwaterMaterial()
{

}

void SubwaterMaterial::setLightPosition(glm::vec3 light)
{
	light_position->Set(light);
}

void SubwaterMaterial::setCameraPosition(glm::vec3 camera_pos)
{
	camera_position->Set(camera_pos);
}

void SubwaterMaterial::set_depth(glm::vec3 position_from_water) {
	depth->Set(position_from_water);
}

void SubwaterMaterial::set_water_pos(glm::vec3 water_position) {
	water_pos->Set(water_position);
}

void SubwaterMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void SubwaterMaterial::update(Node* o,const int elapsedTime)
{
	
	time->Set(time->getValue()+0.1);
	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}