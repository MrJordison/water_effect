#ifndef _SubwaterMaterial_H
#define _SubwaterMaterial_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include "GPUResources/Buffers/GPUArrayBuffer.h"
#include <memory.h>

class SubwaterMaterial : public MaterialGL
{
	public:
		SubwaterMaterial(std::string name);
		~SubwaterMaterial();

		virtual void render(Node *o);
		virtual void update(Node* o,const int elapsedTime);

		void setLightPosition(glm::vec3 light);
		void setCameraPosition(glm::vec3 camera_pos);
		void set_depth(glm::vec3 position_from_water);
		void set_water_pos(glm::vec3 water_position);

	private:
		GPUvec3* light_position ;
		GPUvec3* light_coeffs;
		GPUvec3* camera_position;
		GPUvec4* color;
		GPUmat4* modelViewProj;
		GPUfloat* time;
		GPUvec3* depth;
		GPUvec3* water_pos;

		GPUsampler* sampler;



};

#endif