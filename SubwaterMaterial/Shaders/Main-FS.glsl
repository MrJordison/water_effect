#version 430
//#extension GL_ARB_bindless_texture : enable

#define TAU 6.28318530718
#define MAX_ITER 5

layout(std140) uniform CPU
{	
	vec3 light_coeffs;

	//Code sans Texture		//
	vec4 CPU_color;

	//Code avec Texture		//
	//sampler2D tex;
};

in vec3 v_tex_coord;
in float v_time;
in vec3 normale;
in vec3 vue;
in vec3 light_direction;
in float coeff_depth;

layout (location = 0) out vec4 Color;

void main()
{

	//Rendu des caustiques 

	float f_time = v_time * 0.1 + 23;
	vec2 uv = v_tex_coord.xz / 8;
 
#ifdef SHOW_TILING
	vec2 p = mod(uv*TAU*2.0, TAU)-250.0;
#else
    vec2 p = mod(uv*TAU, TAU)-250.0;
#endif
	vec2 i = vec2(p);
	float c = 1.0;
	float inten = .005;

	for (int n = 0; n < MAX_ITER; n++) 
	{
		float t = f_time * (1.0 - (3.5 / float(n+1)));
		i = p + vec2(cos(t - i.x) + sin(t + i.y), sin(t - i.y) + cos(t + i.x));
		c += 1.0/length(vec2(p.x / (sin(i.x+t)/inten),p.y / (cos(i.y+t)/inten)));
	}
	c /= float(MAX_ITER);
	c = 1.17-pow(c, 1.4);

	vec3 colour = vec3(pow(abs(c), 8.0));

	//	Code avec Texture	//
	//vec3 tex_color = texture(tex, v_tex_coord.xy).xyz;
	//colour = clamp(colour + tex_color, 0.0, 1.0);

	//Code sans Texture		//
    colour = clamp(colour + CPU_color.rgb , 0.0, 1.0);
    

	#ifdef SHOW_TILING
	// Flash tile borders...
	vec2 pixel = 2.0 / 16/9 ;
	uv *= 2.0;

	float f = floor(mod(v_time*.5, 2.0)); 	// Flash value.
	vec2 first = step(pixel, uv) * f;		   	// Rule out first screen pixels and flash.
	uv  = step(fract(uv), pixel);				// Add one line of pixels per tile.
	colour = mix(colour, vec3(1.0, 1.0, 0.0), (uv.x + uv.y) * first.x * first.y); // Yellow line
	
	#endif


	//Assombrissement de la couleur selon coefficient de profondeur

	colour = clamp(colour * max(0.,(1. - coeff_depth)), 0., 1.);


	//Application du mod�le de Phong

	vec4 ambiante_composante = vec4(colour,1.) ; 

	vec3 ld = normalize(light_direction);
	vec3 v = normalize(vue);
	vec3 n = normalize(normale);

	vec3 diffuse_composante = ambiante_composante.rgb * max(dot(n, -ld),0) ;

	// Calcul de la composante sp�culaire //
	vec3 reflect_direction = reflect(ld,n) ;
	vec3 specular_composante = vec3(1.0,1.0,1.0) * pow(max(dot(reflect_direction,v),0),64) ;

	vec3 ambiante_composante_color  = light_coeffs.x * ambiante_composante.rgb ;
	vec3 diffuse_composante_color  = light_coeffs.y * diffuse_composante ;
	vec3 specular_composante_color  = light_coeffs.z * specular_composante ;

	vec3 phong_color = vec3(ambiante_composante_color + diffuse_composante_color + specular_composante_color);


	//Rendu du fragment
	Color = vec4(phong_color, 1.0);
}

