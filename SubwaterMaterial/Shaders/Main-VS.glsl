#version 430

#extension GL_ARB_shading_language_include : enable
//#extension GL_ARB_bindless_texture : enable

#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
	mat4 MVP;
	float time;
	vec3 light_position;
	vec3 camera_position;
	vec3 depth;
	vec3 water_pos;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

	out vec3 v_tex_coord ;
	out float v_time;
	out vec3 normale;
	out vec3 vue;
	out vec3 light_direction;
	out float coeff_depth;

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale ;
layout (location = 3) in vec3 tex_coord ;


vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float noise2D(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

float gen_hauteur(vec2 position){
	vec2 unit_coord = normalize(position);
	float hauteur = 0.8 * noise2D(0.15 * position);

		return hauteur;
}

//Fonction permettant d'obtenir les nouvelles normales apr�s modification de la mesh
vec3 disturbed_normale(vec3 v){
	float epsilon = 0.01;
	
	//Calcul des d�riv�es partielles en x et z
	float xprime, xsecond, zprime, zsecond;

	xprime =  gen_hauteur(vec2(v.x + epsilon, v.z));
	xsecond = gen_hauteur(vec2(v.x - epsilon, v.z));
	zprime =  gen_hauteur(vec2(v.x , v.z + epsilon));
	zsecond =  gen_hauteur(vec2(v.x, v.z  - epsilon));

	vec3 derivx = vec3(1.0, (xprime - xsecond) / (2 * epsilon), 0.);
	vec3 derivz = vec3(0., (zprime - zsecond) / (2 * epsilon), 1.);

	vec3 new_normale = cross(derivz, derivx);
	return normalize(new_normale);	
}

void main(){

	vec3 ptObjPos = Position;
	ptObjPos.y += gen_hauteur(vec2(Position.x,Position.z));
	normale = disturbed_normale(ptObjPos);


	// Calcul des vecteurs de vue et direction de la source d'�clairage pour Phong //
	light_direction = normalize(ptObjPos - light_position) ;
	vue = normalize(camera_position - ptObjPos) ;

	v_time = time;
	v_tex_coord = Position ;
	
	gl_Position = MVP * vec4(ptObjPos,1.);
	coeff_depth = (ptObjPos.y - water_pos.y) / (depth.y - water_pos.y) ;
	
}