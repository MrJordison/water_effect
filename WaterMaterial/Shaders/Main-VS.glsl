#version 430

#extension GL_ARB_shading_language_include : enable
//#extension GL_ARB_bindless_texture : enable

#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
	vec3 light_position;
	vec3 camera_position;
	mat4 MVP;
	float time;
	float speed;
	float amplitude;
	float depth;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

out vec3 light_direction ;
out vec3 vue;
out vec3 normale;
out vec3 v_tex_coord ;
out float hauteur;

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale ;
layout (location = 3) in vec3 tex_coord ;

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

//Bruit de Perlin en 2D, utilis� sur le plan XZ pour modifier la hauteur du vertex
float noise2D(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

//D'apr�s un vecteur 2D, applique un noise Perlin puis un flou gaussien pour adoucir
float gen_hauteur(vec2 position){
	float freq = 3.f;
	float amplitude = 0.1f;

	vec2 unit_coord = normalize(position);

	float hauteur = amplitude * (noise2D(freq * position)
	+ noise2D(freq * (position + vec2(0.1,0)))
	+ noise2D(freq * (position + vec2(0.1,-0.1)))
	+ noise2D(freq * (position + vec2(0.,-0.1)))
	+ noise2D(freq * (position + vec2(-0.1,-0.1)))
	+ noise2D(freq * (position + vec2(-0.1,0)))
	+ noise2D(freq * (position + vec2(-0.1,0.1)))
	+ noise2D(freq * (position + vec2(0.,0.1)))
	+ noise2D(freq * (position + vec2(0.1,0.1))));
	hauteur /= 9.;

		return hauteur;
}

float wave(float x, float z){
	vec2 vertice = vec2(x,z);

	//Simple direction
	vec2 direction = vec2(0.5f,2.f);

	//Circular direction
	/*
	vec2 centre = vec2(1.,	1.);
	vec2 direction = centre - vertice;
	direction = normalize(direction);
	*/


	float res =pow(sin( dot(vertice,direction) +  time*speed)+1.,2.5f);
	return 2.*amplitude * (res / 2.);

}

vec3 disturbed_normale(vec3 v){
	float epsilon = 0.01;
	
	//Calcul des d�riv�es partielles en x et z
	float xprime, xsecond, zprime, zsecond;

	xprime = wave(v.x + epsilon, v.z) + gen_hauteur(vec2(v.x + epsilon, v.z));
	xsecond = wave(v.x - epsilon, v.z) + gen_hauteur(vec2(v.x - epsilon, v.z));
	zprime = wave(v.x , v.z + epsilon) + gen_hauteur(vec2(v.x , v.z + epsilon));
	zsecond = wave(v.x, v.z - epsilon) + gen_hauteur(vec2(v.x, v.z  - epsilon));

	vec3 derivx = vec3(1.0, (xprime - xsecond) / (2 * epsilon), 0.);
	vec3 derivz = vec3(0., (zprime - zsecond) / (2 * epsilon), 1.);

	vec3 new_normale = cross(derivz, derivx);
	return normalize(new_normale);	
}

void main()
{
	//Cr�ation de la mesh de la vague et modification des normales en cons�quence
	vec3 ptObjPos = Position;
	if(ptObjPos.y >=0.){
		ptObjPos.y += wave(ptObjPos.x, ptObjPos.z);
		ptObjPos.y += gen_hauteur(vec2(ptObjPos.x,ptObjPos.z));
		normale = disturbed_normale(ptObjPos);

	}

	//Calcul de la diff�rence de hauteur entre la vague et la position initiale du plan en y pour
	// la variation de couleur de l'eau
	hauteur = ptObjPos.y - Position.y;

	// Calcul des vecteurs de vue et direction de la source d'�clairage pour Phong //
	light_direction = normalize(ptObjPos - light_position) ;
	vue = normalize(camera_position - ptObjPos) ;

	gl_Position = MVP * vec4(ptObjPos,1.0) ;
	
	
	v_tex_coord = tex_coord ;
	
}