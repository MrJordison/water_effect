#version 430
#extension GL_ARB_bindless_texture : enable

layout(std140) uniform CPU
{	
	vec3 light_coeffs;
	vec4 CPU_color;
};


in vec3 light_direction ;
in vec3 vue;
in vec3 normale;
in vec3 v_tex_coord ;
in float hauteur;

layout (location = 0) out vec4 Color;

void main()
{
	//Chargement de la composante ambiante (couleur de base) du fragment
	vec4 ambiante_composante = CPU_color ; 

	//modifie la couleur de base selon la hauteur du vertex r�cup�r�e
	ambiante_composante.xyz += vec3(1.,1.,1.) * (hauteur / 3.);

	vec3 ld = normalize(light_direction);
	vec3 v = normalize(vue);
	vec3 n = normalize(normale);


	vec3 diffuse_composante = ambiante_composante.rgb * max(dot(n, -ld),0) ;

	// Calcul de la composante sp�culaire //
	vec3 reflect_direction = reflect(ld,n) ;
	vec3 specular_composante = vec3(1.0,1.0,1.0) * pow(max(dot(reflect_direction,v),0),128) ;



	vec3 ambiante_composante_color  = light_coeffs.x * ambiante_composante.rgb ;
	vec3 diffuse_composante_color  = light_coeffs.y * diffuse_composante ;
	vec3 specular_composante_color  = light_coeffs.z * specular_composante ;


	Color = vec4(ambiante_composante_color + diffuse_composante_color + specular_composante_color, ambiante_composante.a);

}

