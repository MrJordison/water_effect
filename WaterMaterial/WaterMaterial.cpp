#include "WaterMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


WaterMaterial::WaterMaterial(std::string name,  glm::vec4 & c):
	MaterialGL(name,"WaterMaterial")
{

	light_position = vp->uniforms()->getGPUvec3("light_position");
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	camera_position = vp->uniforms()->getGPUvec3("camera_position");

	color = fp->uniforms()->getGPUvec4("CPU_color");
	light_coeffs = fp->uniforms()->getGPUvec3("light_coeffs");
	time = vp->uniforms()->getGPUfloat("time");
	amplitude = vp->uniforms()->getGPUfloat("amplitude");
	speed = vp->uniforms()->getGPUfloat("speed");

	color->Set(c);
	light_position->Set(glm::vec3(0.,10000,0)); 
	light_coeffs->Set(glm::vec3(0.2, 0.6, 1.0));
	camera_position->Set(glm::vec3(100,50,.0));
	time->Set(0.f);
	amplitude->Set(0.2f);
	speed->Set(1.f);
}
WaterMaterial::~WaterMaterial()
{

}

void WaterMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void WaterMaterial::setLightPosition(glm::vec3 light)
{
	light_position->Set(light);
}

void WaterMaterial::setCameraPosition(glm::vec3 camera_pos)
{
	camera_position->Set(camera_pos);
}

void WaterMaterial::set_amplitude(float a) {
	amplitude->Set(a);
}

void WaterMaterial::set_speed(float s) {
	speed->Set(s);
}

void WaterMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void WaterMaterial::update(Node* o,const int elapsedTime)
{
	
	time->Set(time->getValue()+0.1);



	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}