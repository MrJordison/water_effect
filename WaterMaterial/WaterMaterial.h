#ifndef _WATERMATERIAL_H
#define _WATERMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include "GPUResources/Buffers/GPUArrayBuffer.h"
#include <memory.h>

class WaterMaterial : public MaterialGL
{
	public:
		WaterMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5,0.5,0.5,1.0));
		~WaterMaterial();
		void setColor(glm::vec4 & c);

		virtual void render(Node *o);
		virtual void update(Node* o,const int elapsedTime);

		void setLightPosition(glm::vec3 light);

		void setCameraPosition(glm::vec3 camera_pos);

		void set_amplitude(float a);
		void set_speed(float s);

	private:
		GPUvec3* light_position ;
		GPUvec3* light_coeffs;
		GPUvec3* camera_position;
		GPUvec4* color;
		GPUmat4* modelViewProj;
		GPUfloat* time;
		GPUfloat* amplitude;
		GPUfloat* speed;



};

#endif