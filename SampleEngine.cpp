﻿/*
EngineGL overloaded for custom rendering
*/

#include "SampleEngine.h"
#include "Engine/OpenGL/v4/GLProgram.h"
#include "Engine/OpenGL/SceneLoaderGL.h"
#include "Engine/Base/NodeCollectors/MeshNodeCollector.h"
#include "Engine/Base/NodeCollectors/FCCollector.h"


#include <AntTweakBar/AntTweakBar.h>

#include "Materials/PhongMaterial/PhongMaterial.h"
#include "Materials/TextureMaterial/TextureMaterial.h"
#include "Materials/SimpleTextureMaterial/SimpleTextureMaterial.h"
#include "Materials/WaterMaterial/WaterMaterial.h"
#include "Materials/SubwaterMaterial/SubwaterMaterial.h"



#include "GPUResources/GPUInfo.h"

SampleEngine::SampleEngine (int width, int height) :
EngineGL (width, height)
{
	
}

SampleEngine::~SampleEngine ()
{
}


bool SampleEngine::init ()
{
	LOG (INFO) << "Initializing Scene";
	timeQuery->create ();

	LOG(INFO) << GPUInfo::Instance()->getOpenGLVersion() << std::endl;

	// Load shaders in "\common" into graphic cards
	GLProgram::prgMgr.addPath (ressourceMaterialPath + "Common", "");

	// Create Lighting Model GL and collect all light nodes of the scene 
	lightingModel = new LightingModelGL("LightingModel", scene->getRoot());
	// Bind the light buffer for rendering
	lightingModel->bind();


	/*Node* bunny = scene->getNode("Bunny");
	bunny->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "Bunny.obj"));
	bunny->setMaterial(new TextureMaterial("TextureMaterial",GPUTexture2D(ressourceTexPath + "Bunny1.png"), glm::vec4(0.8,0.8,0.1,1.0)));
	scene->getSceneNode()->adopt( bunny ); 
	bunny->frame()->scale(glm::vec3(6.0));*/

	GPUFBO effect("effect");
	effect.create(w_Width, w_Height, 1, true, GL_RGBA, GL_TEXTURE_2D);
	
	//Création de la skybox
	Node* skybox = scene->getNode("SkyBox");
	skybox->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "skybox.obj"));
	//skybox->setMaterial(new SimpleTextureMaterial("SimpleTextureMaterial", GPUTexture2D(ressourceTexPath + "skybox.jpg")));
	skybox->setMaterial(new SimpleTextureMaterial("SimpleTextureMaterial"));
	scene->getSceneNode()->adopt(skybox);

	//placement de la caméra dans la skybox
	scene->camera()->getFrame()->loadIdentity();
	skybox->frame()->translate(scene->camera()->getFrame()->convertPtTo(glm::vec3(0.), skybox->frame()));
	skybox->frame()->attachTo(scene->camera()->getFrame());
	
	//Création d'un plan sous l'eau et application du shader correspondant
	Node * underwater = scene->getNode("underwater");
	underwater->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "surface.obj"));
	underwater->setMaterial(new SubwaterMaterial("UnderwaterMaterial"));
	scene->getSceneNode()->adopt(underwater);
	underwater->frame()->translate(glm::vec3(0., -5., 0.));
	
	//Ajout de divers modèles 3D pour la déco
	Node * turtle = scene->getNode("turtle");
	turtle->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "turtle.obj"));
	turtle->setMaterial(new PhongMaterial("PhongMaterial", glm::vec4(0.1, 0.45, 0.2, 1.)));
	scene->getSceneNode()->adopt(turtle);
	turtle->frame()->attachTo(underwater->frame());
	turtle->frame()->scale(glm::vec3(0.25));
	turtle->frame()->translate(glm::vec3(-3, 12.,3.));

	Node * grass = scene->getNode("grass");
	grass->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "grass.obj"));
	grass->setMaterial(new PhongMaterial("PhongMaterial", glm::vec4(0.1, 0.45, 0.2, 1.)));
	dynamic_cast<PhongMaterial*>(grass->getMaterial())->set_l_coeffs(glm::vec3(0.4, 0.6, 0.));
	scene->getSceneNode()->adopt(grass);
	grass->frame()->attachTo(underwater->frame());
	grass->frame()->translate(glm::vec3(4., 0., 1.));

	Node * grass2 = scene->getNode("grass2");
	grass2->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "grass.obj"));
	grass2->setMaterial(new PhongMaterial("PhongMaterial", glm::vec4(0.1, 0.45, 0.2, 1.)));
	dynamic_cast<PhongMaterial*>(grass2->getMaterial())->set_l_coeffs(glm::vec3(0.4, 0.6, 0.));
	scene->getSceneNode()->adopt(grass2);
	grass2->frame()->attachTo(underwater->frame());
	grass2->frame()->scale(glm::vec3(0.25, 0.75, 0.5));
	grass2->frame()->translate(glm::vec3(-5.5, 0., -6));

	Node * grass3 = scene->getNode("grass3");
	grass3->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "grass.obj"));
	grass3->setMaterial(new PhongMaterial("PhongMaterial", glm::vec4(0.1, 0.45, 0.2, 1.)));
	dynamic_cast<PhongMaterial*>(grass3->getMaterial())->set_l_coeffs(glm::vec3(0.4, 0.6, 0.));
	scene->getSceneNode()->adopt(grass3);
	grass3->frame()->attachTo(underwater->frame());
	grass3->frame()->scale(glm::vec3(1., 0.5, 1.));
	grass3->frame()->translate(glm::vec3(0.,1,2.5));
	

	//Création d'un plan d'eau avec le shader de rendu d'eau
	Node* water = scene->getNode("water");
	water->setModel(scene->m_Models.get<ModelGL>(ressourceObjPath + "surface.obj"));
	water->setMaterial(new WaterMaterial("WaterMaterial", glm::vec4(0., 0.35, 0.5, 0.7f)));
	scene->getSceneNode()->adopt(water);

	//Gestion des effets de vagues avec TweakBar
	TwBar handler();
	


	// Create Bounding Box Material for bounding box rendering
	boundingBoxMat = new BoundingBoxMaterial ("BoundingBoxMat");

	// OpenGL state variable initialisation
	glClearColor (0.4, 0.4, 0.4, 1.0);
	glEnable (GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);




	// Force window Resize
	this->onWindowResize (w_Width, w_Height);

	allNodes->collect (scene->getRoot ());

	renderedNodes->collect (scene->getRoot ());

	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (0);

	return(true);
}


void SampleEngine::render ()
{
	// Begin Time query
	timeQuery->begin ();
	
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Recherche et affichage de la skybox dans l'ensemble des nodes
	//elle doit être affichée en premier afin d'être "derrière" le reste de la scène
	//en désactivant l'écriture dans le zbuffer
	/*for (unsigned int i = 0; i < renderedNodes->nodes.size(); i++) {
		if (renderedNodes->nodes[i]->getName() == "SkyBox") {
			scene->getNode("SkyBox")->frame()->translate(scene->camera()->getFrame()->convertPtTo(glm::vec3(0.), scene->getNode("SkyBox")->frame()));
			glDepthMask(GL_FALSE);
			renderedNodes->nodes[i]->render();
			glDepthMask(GL_TRUE);
		}
	}*/

	////Rendering every collected node
	for (unsigned int i = 0; i < renderedNodes->nodes.size(); i++) {
		if (renderedNodes->nodes[i]->getName() != "SkyBox")
			renderedNodes->nodes[i]->render();
	}
		
	if (drawLights)
		lightingModel->renderLights();

	if (drawBoundingBoxes)
		for (unsigned int i = 0; i < renderedNodes->nodes.size(); i++)
			renderedNodes->nodes[i]->render(boundingBoxMat);

	// end time Query					
	timeQuery->end ();


	scene->needupdate = false;

}
void SampleEngine::animate (const int elapsedTime)
{
	
	// Animate each node
	for (unsigned int i = 0; i < allNodes->nodes.size (); i++)
		allNodes->nodes[i]->animate (elapsedTime);


	//Animation et modification des attributs des objets
	time++;
	//maj des positions de lumière et caméra pour les différents objets de la scène
	(dynamic_cast<WaterMaterial*>(scene->getNode("water")->getMaterial())->setLightPosition(glm::vec3(-10.f, 50.f, 0.)));
	(dynamic_cast<WaterMaterial*>(scene->getNode("water")->getMaterial())->setCameraPosition(scene->camera()->convertPtTo(glm::vec3(0.), scene->getNode("water")->frame())));
	(dynamic_cast<SubwaterMaterial*>(scene->getNode("underwater")->getMaterial())->setLightPosition(glm::vec3(-10.f, 50.f, 0.)));
	(dynamic_cast<SubwaterMaterial*>(scene->getNode("underwater")->getMaterial())->setCameraPosition(scene->camera()->convertPtTo(glm::vec3(0.), scene->getNode("underwater")->frame())));
	(dynamic_cast<PhongMaterial*>(scene->getNode("turtle")->getMaterial())->setLightPosition(glm::vec3(-10.f, 50.f, 0.)));
	(dynamic_cast<PhongMaterial*>(scene->getNode("turtle")->getMaterial())->setCameraPosition(scene->camera()->convertPtTo(glm::vec3(0.), scene->getNode("turtle")->frame())));
	
	//oscillement vertical dynamique de la tortue en fonction du temps
	scene->getNode("turtle")->frame()->translate(glm::vec3(0.,(float)glm::sin((double)time/20.)*0.03,0.));

	//maj des vecteurs de distances entre la surface de l'eau et le fond de l'eau
	(dynamic_cast<SubwaterMaterial*>(scene->getNode("underwater")->getMaterial())->set_depth(scene->getNode("water")->frame()->convertPtTo(glm::vec3(0.f, -7., 0.f), scene->getNode("underwater")->frame())));
	(dynamic_cast<SubwaterMaterial*>(scene->getNode("underwater")->getMaterial())->set_water_pos(scene->getNode("water")->frame()->convertPtTo(glm::vec3(0.f), scene->getNode("underwater")->frame())));

	/*
	(dynamic_cast<TextureMaterial*>(scene->getNode("Bunny")->getMaterial())->setLightPosition(glm::vec3(5. * cos(time / 20), sin(time / 20) * 5, 0.)));
	(dynamic_cast<TextureMaterial*>(scene->getNode("Bunny")->getMaterial())->setCameraPosition(scene->camera()->convertPtTo(glm::vec3(0.), scene->getNode("Bunny")->frame())));
	*/
}

void SampleEngine::onWindowResize (int width, int height)
{

	glViewport (0, 0, width, height);
	scene->camera ()->setAspectRatio ((float)width / (float)height);
	w_Width = width;
	w_Height = height;


}



