#include "PhongMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


PhongMaterial::PhongMaterial(std::string name, glm::vec4 & c):
	MaterialGL(name,"PhongMaterial")
{
	light_position = vp->uniforms()->getGPUvec3("light_position");
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	camera_position = vp->uniforms()->getGPUvec3("camera_position");
	color = fp->uniforms()->getGPUvec4("CPU_color");
	light_coeffs = fp->uniforms()->getGPUvec3("light_coeffs");


	color->Set(c);
	light_position->Set(glm::vec3(0.,10000,0)); 
	light_coeffs->Set(glm::vec3(0.7, 0.3, 0.45));
	camera_position->Set(glm::vec3(100,50,.0));

}
PhongMaterial::~PhongMaterial()
{

}

void PhongMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void PhongMaterial::setLightPosition(glm::vec3 light)
{
	light_position->Set(light);
}

void PhongMaterial::setCameraPosition(glm::vec3 camera_pos)
{
	camera_position->Set(camera_pos);
}

void PhongMaterial::set_l_coeffs(glm::vec3 coeffs) {
	light_coeffs->Set(coeffs);
}

void PhongMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void PhongMaterial::update(Node* o,const int elapsedTime)
{
	//time->Set(elapsedTime);

	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}