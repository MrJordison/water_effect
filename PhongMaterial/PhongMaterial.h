#ifndef _PHONGMATERIAL_H
#define _PHONGMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include "GPUResources/Buffers/GPUArrayBuffer.h"
#include <memory.h>

class PhongMaterial : public MaterialGL
{
	public:
		PhongMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5,0.5,0.5,1.0));
		~PhongMaterial();
		void setColor(glm::vec4 & c);

		virtual void render(Node *o);
		virtual void update(Node* o,const int elapsedTime);

		void setLightPosition(glm::vec3 light);
		void setCameraPosition(glm::vec3 camera_pos);
		void set_l_coeffs(glm::vec3 coeffs);

	private:
		GPUvec3* light_position ;
		GPUvec3* light_coeffs;
		GPUvec3* camera_position;
		GPUvec4* color;
		GPUmat4* modelViewProj;
};

#endif