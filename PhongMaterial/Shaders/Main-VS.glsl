#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
	vec3 light_position;
	vec3 camera_position;
	mat4 MVP;
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };

out vec3 light_direction ;
out vec3 vue;
out vec3 normale;

layout (location = 0) in vec3 Position;
layout (location = 2) in vec3 Normale ;

void main()
{
	// Calcul de la composante diffuse //
	light_direction = normalize(Position.xyz - light_position) ;

	vue = normalize(camera_position - Position) ;

	gl_Position = MVP * vec4(Position,1.0) ;
	normale = Normale ;

	
}