#version 430

#extension GL_ARB_shading_language_include : enable


layout(std140) uniform CPU
{	
	vec3 light_coeffs;
	vec4 CPU_color;
};


in vec3 light_direction ;
in vec3 vue;
in vec3 normale;

layout (location = 0) out vec4 Color;

void main()
{
	vec4 ambiante_composante = CPU_color;

	vec3 ld = normalize(light_direction);
	vec3 v = normalize(vue);
	vec3 n = normalize(normale);

	vec3 diffuse_composante = CPU_color.rgb * max(dot(n, -ld),0) ;

	// Calcul de la composante spéculaire //
	vec3 reflect_direction = reflect(ld,n) ;
	vec3 specular_composante = vec3(1.0,1.0,1.0) * pow(max(dot(reflect_direction,v),0),64) ;



	vec3 ambiante_composante_color  = light_coeffs.x * ambiante_composante.rgb ;
	vec3 diffuse_composante_color  = light_coeffs.y * diffuse_composante ;
	vec3 specular_composante_color  = light_coeffs.z * specular_composante ;

	Color = vec4(ambiante_composante_color + diffuse_composante_color + specular_composante_color, ambiante_composante.a);
}